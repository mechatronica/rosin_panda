/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2012, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Ioan Sucan, Ridhwan Luthra*/

/**********************************************************************
 * This example is an altered version of the pick & place example from
 * MoveIt and is used for the ROS Industrial workshop at 
 * Saxion Mechatronics
 * 
 * Usefull background information:
 * 
 * ROS Industrial training materials: https://industrial-training-master.readthedocs.io/en/melodic/
 * MoveIt tutorials: https://ros-planning.github.io/moveit_tutorials/
 * MoveIt overview and concepts: https://moveit.ros.org/documentation/concepts/ 
 * Class reference - MoveGroupInterface: http://docs.ros.org/en/melodic/api/moveit_ros_planning_interface/html/classmoveit_1_1planning__interface_1_1MoveGroupInterface.html
 * Class reference - PlanningSceneInterface: http://docs.ros.org/en/melodic/api/moveit_ros_planning_interface/html/classmoveit_1_1planning__interface_1_1PlanningSceneInterface.html
 * Class reference - PlanningScene: http://docs.ros.org/en/melodic/api/moveit_core/html/classplanning__scene_1_1PlanningScene.html
 * Class reference - AllowedCollisionMatrix: http://docs.ros.org/en/melodic/api/moveit_core/html/classcollision__detection_1_1AllowedCollisionMatrix.html
 * MoveIt message definitions: http://docs.ros.org/en/melodic/api/moveit_msgs/html/index-msg.html
 * Geometry message definitions: http://docs.ros.org/en/melodic/api/geometry_msgs/html/index-msg.html
 * 
 *********************************************************************/

// ROS
#include <ros/ros.h>
#include <ros/console.h>

// MoveIt
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_msgs/GetPlanningScene.h>
#include <moveit_msgs/PlanningScene.h>
#include <moveit_msgs/PlanningSceneWorld.h>
#include <moveit_msgs/ApplyPlanningScene.h>

// Shapes and meshes
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <geometric_shapes/shapes.h>
#include <geometric_shapes/shape_operations.h>
#include <geometric_shapes/shape_messages.h>
#include <geometric_shapes/mesh_operations.h>
#include <geometry_msgs/PoseStamped.h>
#include <std_msgs/Empty.h>

// Datatypes and structures
#include <string>
#include <iostream>
#include <vector>

// Functions for pickplace
void openGripper(trajectory_msgs::JointTrajectory& posture);
void closedGripper(trajectory_msgs::JointTrajectory& posture);
void pick(moveit::planning_interface::MoveGroupInterface& move_group);
void place(moveit::planning_interface::MoveGroupInterface& move_group);
moveit_msgs::PlanningScene createStaticScene();
moveit_msgs::PlanningScene getMagicNewBottle();


int main(int argc, char** argv)
{
  ros::init(argc, argv, "panda_arm_pick_place");
  ros::NodeHandle nh;
  ros::AsyncSpinner spinner(1);
  spinner.start();

  // Define the move group to use
  static const std::string PLANNING_GROUP = "panda_arm";

  // Visualization tool for debugging purposes
  moveit_visual_tools::MoveItVisualTools visual_tools("panda_link0");
  visual_tools.deleteAllMarkers();

  // Wait a bit for everything to settle
  ros::WallDuration(1.0).sleep();

  // Remote control is an introspection tool that allows users to step through a high level script
  // via buttons and keyboard shortcuts in RViz
  visual_tools.loadRemoteControl();

  // MoveGroup and PlanningScene interfaces
  moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
  moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);
  move_group.setPlanningTime(10.0);
  move_group.setNumPlanningAttempts(5);

  // Set the arm in a predefined joint space configuration as defined in the srdf file.
  move_group.setNamedTarget("ready");
  move_group.move();

  // Very usefull functions to get the end effector pose
  geometry_msgs::PoseStamped end_effector_link_pose;
  end_effector_link_pose = move_group.getCurrentPose();

  ROS_INFO_STREAM("Current end-effector link: " << move_group.getEndEffectorLink() << ", "
    << "current end-effector name: " << move_group.getEndEffector());

  ROS_INFO_STREAM("End effector link position in " << end_effector_link_pose.header.frame_id << " frame: "
    << "X: " << end_effector_link_pose.pose.position.x
    << ", Y: " << end_effector_link_pose.pose.position.x
    << ", Z: " << end_effector_link_pose.pose.position.x);

  std::vector<double> end_effector_link_RPY;
  end_effector_link_RPY = move_group.getCurrentRPY();
  ROS_INFO_STREAM("End effector link RPY: "
    << "Roll: " << end_effector_link_RPY[0]
    << ", Pitch: " << end_effector_link_RPY[1]
    << ", Yaw: " << end_effector_link_RPY[2]);

  // Create a planning scene diff publisher to update the planning scene
  ros::Publisher planning_scene_diff_publisher;
  planning_scene_diff_publisher = nh.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
  moveit_msgs::PlanningScene planning_scene_msg;
  ros::WallDuration sleep_t(0.5);
  while (planning_scene_diff_publisher.getNumSubscribers() < 1)
  {
    sleep_t.sleep();
  }

  // Create a planning scene diff service client to update the planning scene
  ros::ServiceClient planning_scene_diff_client;
  planning_scene_diff_client = nh.serviceClient<moveit_msgs::ApplyPlanningScene>("apply_planning_scene");
  planning_scene_diff_client.waitForExistence();
  moveit_msgs::ApplyPlanningScene applyPlanningSceneSrv;

  // Build a (static) world scene
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  visual_tools.prompt("Press 'next' in the RvizVisualToolsGui to load the scene");

  planning_scene_msg = createStaticScene();
  // Use a diff service call (sychronous):
  applyPlanningSceneSrv.request.scene = planning_scene_msg;
  planning_scene_diff_client.call(applyPlanningSceneSrv);

  // Allow collision between the base links of the arm and the desk
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  visual_tools.prompt("Press 'next' in the RvizVisualToolsGui to change the ACM");
  collision_detection::AllowedCollisionMatrix acm;
  // Use a service request to get the allowed collision matix.
  // Alternatives are using the getAllowedCollisionMatrix() from the planning scene interface
  // or using the planningscene monitor.
  ros::ServiceClient planningSceneService = nh.serviceClient<moveit_msgs::GetPlanningScene>("/get_planning_scene");
  moveit_msgs::GetPlanningScene getPlanningSceneSrv;
  getPlanningSceneSrv.request.components.components = 128;  // We are only interested in the ACM.

  // Get current ACM
  if (planningSceneService.call(getPlanningSceneSrv))
  {
    acm = getPlanningSceneSrv.response.scene.allowed_collision_matrix;
    ROS_INFO_STREAM("Default Allowed Collision Matrix (ACM)");
    acm.print(std::cout);  // Print the ACM
  }
  else
  {
    ROS_WARN("Failed to call service /get_planning_scene");
  }

  // Change ACM: allow collision between desk1 and the base links of the Panda arm
  acm.setEntry("panda_link0", "desk1", true);
  acm.setEntry("panda_link1", "desk1", true);

  // Publish the changed ACM
  moveit_msgs::AllowedCollisionMatrix acm_msg;
  acm.getMessage(acm_msg);
  planning_scene_msg.allowed_collision_matrix = acm_msg;
  planning_scene_msg.is_diff = true;
  planning_scene_diff_publisher.publish(planning_scene_msg);

  // Get updated ACM and print
  if (planningSceneService.call(getPlanningSceneSrv))
  {
    acm = getPlanningSceneSrv.response.scene.allowed_collision_matrix;
    ROS_INFO_STREAM("Updated Allowed Collision Matrix (ACM)");
    acm.print(std::cout);  // Print the ACM
  }
  else
  {
    ROS_WARN("Failed to call service /get_planning_scene");
  }

  // Get a bottle to pick up
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  visual_tools.prompt("Press 'next' in the RvizVisualToolsGui to order a bottle");

  planning_scene_msg = getMagicNewBottle();
  // Use a diff service call (sychronous):
  applyPlanningSceneSrv.request.scene = planning_scene_msg;
  planning_scene_diff_client.call(applyPlanningSceneSrv);

  // Pick up the bottle
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  visual_tools.prompt("Press 'next' in the RvizVisualToolsGui to start the pick action");
  pick(move_group);

  ros::WallDuration(1.0).sleep();

  // Place the bottle
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  visual_tools.prompt("Press 'next' in the RvizVisualToolsGui to start the place action");
  place(move_group);

  ros::WallDuration(5.0).sleep();

  // Clean up the plan scene so you can leave ROS (MoveIt) and Rviz running
  // and run this node multiple times.
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  visual_tools.prompt("Press 'next' in the RvizVisualToolsGui to remove ");

  // Create vector to hold 4 collision object messages.
  std::vector<moveit_msgs::CollisionObject> collision_objects;
  collision_objects.resize(4);
  collision_objects[0].id = "desk1";
  collision_objects[0].operation = collision_objects[0].REMOVE;
  collision_objects[1].id = "desk2";
  collision_objects[1].operation = collision_objects[1].REMOVE;
  collision_objects[2].id = "screen";
  collision_objects[2].operation = collision_objects[2].REMOVE;
  collision_objects[3].id = "bottle";
  collision_objects[3].operation = collision_objects[3].REMOVE;
  planning_scene_msg.world.collision_objects = collision_objects;

  // Change ACM: remove the allowed collision between desk1 and the base links of the Panda arm
  // The acm object still contains the latest (adapted) ACM so no need to request a new one.
  acm.removeEntry("desk1");  // This removes all pairs that contains this link
  acm.getMessage(acm_msg);  // Creat a message type
  planning_scene_msg.allowed_collision_matrix = acm_msg;

  // The planning scene has changed
  planning_scene_msg.is_diff = true;
  applyPlanningSceneSrv.request.scene = planning_scene_msg;
  planning_scene_diff_client.call(applyPlanningSceneSrv);

  ros::shutdown();  // Shutdown the node
  // ros::waitForShutdown();  // Wait for ctrl^c before shutting down the node
  return 0;
}

void openGripper(trajectory_msgs::JointTrajectory& posture)
{
  // BEGIN_SUB_TUTORIAL open_gripper
  // Add both finger joints of panda robot.
  posture.joint_names.resize(2);
  posture.joint_names[0] = "panda_finger_joint1";
  posture.joint_names[1] = "panda_finger_joint2";

  // Set them as open, wide enough for the object to fit.
  posture.points.resize(1);
  posture.points[0].positions.resize(2);
  posture.points[0].positions[0] = 0.03;
  posture.points[0].positions[1] = 0.03;
  posture.points[0].time_from_start = ros::Duration(0.5);
}

void closedGripper(trajectory_msgs::JointTrajectory& posture)
{
  // Add both finger joints of panda robot.
  posture.joint_names.resize(2);
  posture.joint_names[0] = "panda_finger_joint1";
  posture.joint_names[1] = "panda_finger_joint2";

  // Set them as closed.
  posture.points.resize(1);
  posture.points[0].positions.resize(2);
  posture.points[0].positions[0] = 0.01;
  posture.points[0].positions[1] = 0.01;
  posture.points[0].time_from_start = ros::Duration(0.5);
}

void pick(moveit::planning_interface::MoveGroupInterface& move_group)
{
  // Create a vector of grasps to be attempted, currently only creating single grasp.
  // This is essentially useful when using a grasp generator to generate and test multiple grasps.
  std::vector<moveit_msgs::Grasp> grasps;
  grasps.resize(1);

  // Setting grasp pose
  // ++++++++++++++++++++++
  // This is the pose of panda_link8. |br|
  // Make sure that when you set the grasp_pose, you are setting it to be the pose of the last link in
  // your manipulator which in this case would be `"panda_link8"` You will have to compensate for the
  // transform from `"panda_link8"` to the palm of the end effector.
  grasps[0].grasp_pose.header.frame_id = "bottle";
  tf2::Quaternion orientation;
  orientation.setRPY(M_PI, 0, -M_PI / 4);  // gripper aligned with the y-axis of the base frame
  grasps[0].grasp_pose.pose.orientation = tf2::toMsg(orientation);
  grasps[0].grasp_pose.pose.position.x = 0;
  grasps[0].grasp_pose.pose.position.y = 0;
  grasps[0].grasp_pose.pose.position.z = 0.315;

  // Setting pre-grasp approach
  // ++++++++++++++++++++++++++
  // Defined with respect to frame_id
  grasps[0].pre_grasp_approach.direction.header.frame_id = "bottle";
  // The pre-grasp approach ends at the specified grasp_pose position.
  grasps[0].pre_grasp_approach.direction.vector.z = -1.0;
  grasps[0].pre_grasp_approach.min_distance = 0.05;
  grasps[0].pre_grasp_approach.desired_distance = 0.1;

  // Setting post-grasp retreat
  // ++++++++++++++++++++++++++
  // Defined with respect to frame_id
  grasps[0].post_grasp_retreat.direction.header.frame_id = "bottle";
  grasps[0].post_grasp_retreat.direction.vector.z = 1.0;
  grasps[0].post_grasp_retreat.min_distance = 0.05;
  grasps[0].post_grasp_retreat.desired_distance = 0.20;

  // Setting posture of eef before grasp
  // +++++++++++++++++++++++++++++++++++
  openGripper(grasps[0].pre_grasp_posture);

  // Setting posture of eef during grasp
  // +++++++++++++++++++++++++++++++++++
  closedGripper(grasps[0].grasp_posture);

  // Allow the object to touch the support surface
  move_group.setSupportSurfaceName("desk1");

  // Call pick to pick up the object using the grasps given
  move_group.pick("bottle", grasps);
}

void place(moveit::planning_interface::MoveGroupInterface& move_group)
{
  // TODO(@ridhwanluthra) - Calling place function may lead to "All supplied place locations failed. Retrying last
  // location in verbose mode." This is a known issue. |br|
  // |br|
  // Ideally, you would create a vector of place locations to be attempted although in this example, we only create
  // a single place location.
  std::vector<moveit_msgs::PlaceLocation> place_location;
  place_location.resize(1);

  // Setting place location pose
  // +++++++++++++++++++++++++++
  place_location[0].place_pose.header.frame_id = "panda_link0";
  tf2::Quaternion orientation;
  orientation.setRPY(0, 0, M_PI / 2);
  place_location[0].place_pose.pose.orientation = tf2::toMsg(orientation);

  // For place location, we set the value to the exact location of the center of the object.
  place_location[0].place_pose.pose.position.x = 0.55;
  place_location[0].place_pose.pose.position.y = 0.4;
  place_location[0].place_pose.pose.position.z = 0.0;

  // Setting pre-place approach
  // ++++++++++++++++++++++++++
  // Defined with respect to frame_id
  place_location[0].pre_place_approach.direction.header.frame_id = "panda_link0";
  place_location[0].pre_place_approach.direction.vector.z = -1.0;
  place_location[0].pre_place_approach.min_distance = 0.05;
  place_location[0].pre_place_approach.desired_distance = 0.15;

  // Setting post-grasp retreat
  // ++++++++++++++++++++++++++
  // Defined with respect to frame_id
  place_location[0].post_place_retreat.direction.header.frame_id = "panda_link0";
  place_location[0].post_place_retreat.direction.vector.z = 1.0;
  place_location[0].post_place_retreat.min_distance = 0.05;
  place_location[0].post_place_retreat.desired_distance = 0.15;

  // Setting posture of eef after placing object
  // +++++++++++++++++++++++++++++++++++++++++++
  // Similar to the pick case
  openGripper(place_location[0].post_place_posture);

  // Allow the object to touch the support surface
  move_group.setSupportSurfaceName("desk2");

  // Call place to place the object using the place locations given.
  move_group.place("bottle", place_location);
}

moveit_msgs::PlanningScene createStaticScene()
{
  // Create a message to hold the changes to the planning scene:
  moveit_msgs::PlanningScene planning_scene_msg;

  // Create vector to hold 3 collision object messages.
  std::vector<moveit_msgs::CollisionObject> collision_objects;
  collision_objects.resize(3);

  // Add a table to support the Franka Emika.
  // ++++++++++++++++++++++++++++++++++++++++
  // Create a mesh shape from an stl file with optional scaling.
  shape_msgs::Mesh mesh_msg;  // Message that defines a mesh shape.
  shapes::ShapeMsg shape_msg;  // ShapeMsg is a type that can hold various shape message types.
  shapes::Mesh* mesh;  // Pointer to a mesh shape.
  Eigen::Vector3d scale(1, 1, 1);
  mesh = shapes::createMeshFromResource("package://panda_pickplace/meshes/desk.STL",  scale);
  shapes::constructMsgFromShape(mesh, shape_msg);  // Construct a general shape message.
  mesh_msg = boost::get<shape_msgs::Mesh>(shape_msg);  // Create a Mesh shape message.

  collision_objects[0].id = "desk1";
  collision_objects[0].header.frame_id = "world";

  // Define the shape and its dimensions.
  collision_objects[0].meshes.resize(1);
  collision_objects[0].meshes[0] = mesh_msg;

  // Define the pose of the table.
  collision_objects[0].mesh_poses.resize(1);
  collision_objects[0].mesh_poses[0].position.x = -0.9;
  collision_objects[0].mesh_poses[0].position.y = -0.25;
  collision_objects[0].mesh_poses[0].position.z = 0.0;
  // Convert from Roll Pitch and Yaw to quaternion as it is easier to visualize.
  tf2::Quaternion objectQuaternion;
  // objectQuaternion.setRPY(0, 0, 0.5*M_PI);
  objectQuaternion.setRPY(0, 0, 0);
  tf2::convert(objectQuaternion, collision_objects[0].mesh_poses[0].orientation);

  collision_objects[0].operation = collision_objects[0].ADD;


  // Add a second table where we will be placing the bottle.
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++
  collision_objects[1].id = "desk2";
  collision_objects[1].header.frame_id = "world";

  // Define the shape and its dimensions.
  collision_objects[1].meshes.resize(1);
  collision_objects[1].meshes[0] = mesh_msg;

  // Define the pose of the table.
  collision_objects[1].mesh_poses.resize(1);
  collision_objects[1].mesh_poses[0].position.x = 1.0;
  collision_objects[1].mesh_poses[0].position.y = -0.5;
  collision_objects[1].mesh_poses[0].position.z = 0.0;
  // Convert from Roll Pitch and Yaw to quaternion as it is easier to visualize.
  objectQuaternion.setRPY(0, 0, 0.5*M_PI);
  tf2::convert(objectQuaternion, collision_objects[1].mesh_poses[0].orientation);

  collision_objects[1].operation = collision_objects[1].ADD;


  // Add a splash screen
  // +++++++++++++++++++
  collision_objects[2].id = "screen";
  collision_objects[2].header.frame_id = "world";

  // Define the primitive and its dimensions.
  collision_objects[2].primitives.resize(1);
  collision_objects[2].primitives[0].type = collision_objects[3].primitives[0].BOX;
  collision_objects[2].primitives[0].dimensions.resize(3);
  collision_objects[2].primitives[0].dimensions[0] = 0.5;
  collision_objects[2].primitives[0].dimensions[1] = 0.01;
  collision_objects[2].primitives[0].dimensions[2] = 0.4;

  // Define the pose of the splash screen.
  collision_objects[2].primitive_poses.resize(1);
  collision_objects[2].primitive_poses[0].position.x = 0.75;
  collision_objects[2].primitive_poses[0].position.y = 0;
  collision_objects[2].primitive_poses[0].position.z = 0.2;
  collision_objects[2].primitive_poses[0].orientation.w = 1.0;

  collision_objects[2].operation = collision_objects[2].ADD;

  // Update the planning scene message
  planning_scene_msg.world.collision_objects = collision_objects;

  // You can give the objects some colours other than the default.
  moveit_msgs::ObjectColor object_color;
  object_color.id = "desk1";
  object_color.color.r = 100;
  object_color.color.g = 100;
  object_color.color.b = 100;
  object_color.color.a = 1.0;
  planning_scene_msg.object_colors.push_back(object_color);

  object_color.id = "desk2";
  object_color.color.r = 100;
  object_color.color.g = 100;
  object_color.color.b = 100;
  object_color.color.a = 1.0;
  planning_scene_msg.object_colors.push_back(object_color);

  // Green screen
  object_color.id = "screen";
  object_color.color.r = 0;
  object_color.color.g = 100;
  object_color.color.b = 0;
  object_color.color.a = 0.5;
  planning_scene_msg.object_colors.push_back(object_color);

  // The planning scene has changed
  planning_scene_msg.is_diff = true;

  return planning_scene_msg;
}

moveit_msgs::PlanningScene getMagicNewBottle()
{
  // Create a message to hold the changes to the planning scene:
  moveit_msgs::PlanningScene planning_scene_msg;

  // Create a message to hold a collision object.
  moveit_msgs::CollisionObject bottle;

  // Create a mesh shape from an stl file with optional scaling.
  shape_msgs::Mesh mesh_msg;  // Message that defines a mesh shape.
  shapes::ShapeMsg shape_msg;  // ShapeMsg is a type that can hold various shape message types.
  shapes::Mesh* mesh;  // Pointer to a mesh shape.
  Eigen::Vector3d scale(1, 1, 1);
  mesh = shapes::createMeshFromResource("package://panda_pickplace/meshes/bottle.STL",  scale);
  shapes::constructMsgFromShape(mesh, shape_msg);  // Construct a general shape message.
  mesh_msg = boost::get<shape_msgs::Mesh>(shape_msg);  // Create a Mesh shape message.

  bottle.id = "bottle";
  bottle.header.frame_id = "bottle";

  // Define the shape and its dimensions.
  bottle.meshes.push_back(mesh_msg);

  // Define the pose of the bottle.
  geometry_msgs::Pose pose;
  pose.position.x = 0;
  pose.position.y = 0;
  pose.position.z = 0;
  // Convert from Roll Pitch and Yaw to quaternion as it is easier to visualize.
  tf2::Quaternion objectQuaternion;
  objectQuaternion.setRPY(0, 0, 0);
  tf2::convert(objectQuaternion, pose.orientation);
  bottle.mesh_poses.push_back(pose);

  bottle.operation = bottle.ADD;

  // Update the planning scene message
  planning_scene_msg.world.collision_objects.push_back(bottle);

  // The planning scene has changed
  planning_scene_msg.is_diff = true;

  return planning_scene_msg;
}
