## ROSIN project
![ROSIN](https://bitbucket.org/mechatronica/slides-and_general-info/raw/c7c4af82fa32ff83d3c38bc973282c4c4e277e38/images/rosin_logo.png)

Development of the materials was part of the Education Project ROS Industrial Training Center @ Saxion and was supported by ROSIN - ROS-Industrial Quality-Assured Robot Software Components.  
More information: [rosin-project.eu](http://rosin-project.eu)

![EU Flag](https://bitbucket.org/mechatronica/slides-and_general-info/raw/c7c4af82fa32ff83d3c38bc973282c4c4e277e38/images/eu_flag.jpg)

This project has received funding from the European Union's Horizon 2020  
research and innovation programme under grant agreement no. 732287.